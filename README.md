Calculator - application that perform calculations from list of instructions specified in file.

External libraries:
guava 19.0
lombok 1.16.6
mockito 1.10.19
assertj 3.3.0
junit 4.12
org.apache.commons-lang3 3.0
org.apache.commons-io 1.3.2

Requirements:
    to build:
        JDK 8 or higher
        Maven 3 or higher
    to run:
        JRE 8

Path to build jar:
{project_name}\target\org.bambi.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar

You can rename 'org.bambi.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar' file as you want.

--- Usage from command line:
java -jar org.bambi.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar args
args - Full path to files with operatrions, eg. filePath1 filePath2 ... filePathN
--- Available operations (upper cases are ignored):
    |-- ADD
    |-- SUBSTRACT
    |-- MULTIPLY
    |-- DIVIDE
    |-- APPLY - apply operations to specific number.
    Syntax: <operator> <number>, eg. ADD 6
--- Example file: example.txt

        add 2
        multiply 10
        apply 30
    - end of content

--- Run with command line: java -jar org.bambi.calculator-1.0-SNAPSHOT-jar-with-dependencies.jar C:/example.txt