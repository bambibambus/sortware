package org.calculator.application;

import org.calculator.logic.CalculateOperation;
import org.calculator.messages.Message;

import java.util.Arrays;
import java.util.Collection;

/*
* This class checks if input is correct
* */
public class CalculatorApplication {

    private Collection<String> fileNames;

    public CalculatorApplication(String[] args) {
        this.fileNames = Arrays.asList(args);
    }

    /*
    * If arguments list is empty print message, else run calculator
    * */
    public void run() {
        if(fileNames.isEmpty())
            new Message().printUsage();
        else
            new CalculateOperation(fileNames).operate();

    }
}
