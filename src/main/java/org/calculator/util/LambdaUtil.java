package org.calculator.util;

import java.io.IOException;
import java.util.function.Supplier;
public abstract class LambdaUtil {

    public static <T> T tryOrThrowRuntime(Supplier<T> supplier){
        try{
            return supplier.get();
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
    }

    public static <T> T tryNumericOrThrowRuntime(Supplier<T> supplier){
        try{
            return supplier.get();
        }catch(IllegalArgumentException e){
            System.out.println("Arithmentic exception, cannot divide by zero.");
            throw new RuntimeException(e);
        }
    }

}
