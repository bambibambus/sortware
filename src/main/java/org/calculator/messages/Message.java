package org.calculator.messages;

import com.google.common.base.CharMatcher;
import lombok.NoArgsConstructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.InputStream;
import java.util.Collection;
import java.util.List;

/*
* Class to print messages to user
* */
@NoArgsConstructor
public class Message {
    private final static String RESULT_FROM_FILE = "Result set from file: ";
    private final static String RESULT = "Result number %d: %f %n";

    public static void  printResults(String filename, Collection<Number> results){
        System.out.println(RESULT_FROM_FILE + filename);
        if(!results.isEmpty()) {
            for (int i = 0; i < results.size(); i++) {
                System.out.printf(RESULT, i + 1, ((List) results).get(i));
            }
        }else{
            System.out.println("There is no result, file is empty or is not filled properly");
        }
    }

    public void printUsage(){
        System.out.println(getUsageFileContent());
    }

    private String getUsageFileContent() {
        return tryOpenOrThrowRuntime("/usage_message.txt");
    }

    public String tryOpenOrThrowRuntime(String fileName){
        String result = StringUtils.EMPTY;
        try{
            InputStream is = Message.class.getResourceAsStream(fileName);
            if(is != null) {
                result = IOUtils.toString(is, "utf-8");
            }
            is.close();
        }catch (Throwable e){
            throw new RuntimeException(e);
        }
        return result;
    }

}
