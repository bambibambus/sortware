package org.calculator.logic;

import com.google.common.collect.Lists;
import lombok.Getter;
import org.calculator.input.DataFromFile;
import org.calculator.input.ReadData;
import org.calculator.messages.Message;
import org.calculator.operations.ApplyOperation;
import org.calculator.operations.BaseOperation;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


public class CalculateOperation {

    private static OperationFactory operationFactory;
    @Getter
    private Collection<String> fileNames;

    public CalculateOperation(Collection<String> fileNames) {
        this.fileNames = fileNames;
        if(this.operationFactory == null)
            this.operationFactory = new OperationFactory();
    }

    /*
    * Create BaseOperation object
    * */
    public static BaseOperation createLinkedOperations(Collection listOFEntries) {
        //Lasdt entry should be APPLY
        Optional<BaseOperation> operations = Optional.empty();
        if(ReadData.isApplyLastOperation(listOFEntries)){
            Entry lastEntry = (Entry)ReadData.getLastEntryOptional(listOFEntries).get();
            operations = operationFactory.get(Optional.<BaseOperation>empty(),
                    lastEntry.getKey(),
                    lastEntry.getValue());
            Iterator iterator = listOFEntries.iterator();
            while(iterator.hasNext() && !iterator.equals(lastEntry)){
                Entry currentEntry = (Entry)iterator.next();
                if(iterator.hasNext()) {
                    operations = operationFactory.get(operations, currentEntry.getKey(), currentEntry.getValue());
                }
            }
        }
        return operations.get();
    }

    public void operate() {
        fileNames.forEach(fileName -> {
            Message.printResults(fileName, calculateFromInput(new DataFromFile(fileName)));
        });
    }

    public Collection<Collection<Entry>> splitByApply(Collection<Entry> input) {
        Collection<Integer> indexes = input
                .stream()
                .filter(entry -> entry.getKey().equals(Operator.APPLY))
                .map(((List) input)::indexOf)
                .collect(Collectors.toList());

        Collection result = Lists.newArrayList();

        if(!indexes.isEmpty()){
            Integer from = 0;
            Integer to = 0;
            for(Integer i : indexes){
                to = i + 1;
                result.add(((List) input).subList(from,to));
                from = to;
            }
        }
        return result;
    }

    public Collection<Number> calculateFromInput(DataFromFile dataFromFile) {
        Collection<Entry> operatorCollection = ReadData.getAsOperatorCollection(dataFromFile);
        Collection<Collection<Entry>> splittedEntries = splitByApply(operatorCollection);
        return splittedEntries
                .stream()
                .map((listOFEntries) -> CalculateOperation.createLinkedOperations(listOFEntries))
                .map(BaseOperation::invoke)
                .collect(Collectors.toList());
    }
}
