package org.calculator.logic;

import lombok.NoArgsConstructor;
import org.calculator.operations.*;

import java.util.Optional;


@NoArgsConstructor
public class OperationFactory {

    /*
    * Factory method
    * @params Optional<BaseOperation> , Object , Object
    * @return Optional<BaseOperation>
    * */
    public Optional<BaseOperation> get(Optional<BaseOperation> previousOperation, Object arg1, Object arg2) {
        Operator operator = (Operator) arg1;
        Number parameter = (Number) arg2;
        BaseOperation operation = null;
        switch(operator){
            case APPLY:
                 operation = new ApplyOperation(Optional.empty(),parameter);
                break;
            case ADD:
                operation = new AddOperation(previousOperation,parameter);
                break;
            case SUBSTRACT:
                operation = new SubstractOperation(previousOperation,parameter);
                break;
            case MULTIPLY:
                operation = new MultiplyOperation(previousOperation,parameter);
                break;
            case DIVIDE:
                operation = new DivideOperation(previousOperation,parameter);
                break;
            default:
                break;
        }
        return Optional.ofNullable(operation);
    }
}
