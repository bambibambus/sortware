package org.calculator.operations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/*
* Add operator class
* */
@NoArgsConstructor
@AllArgsConstructor
public class AddOperation implements BaseOperation{

    private Optional<BaseOperation> previousOperation;
    @Getter
    private Number value;

    public AddOperation(Optional previousOperation){
        this.previousOperation = previousOperation;
    }

    @Override
    public Number invoke() {
        return this.previousOperation.isPresent() ? (Float)this.previousOperation.get().invoke() + (Float)this.value : this.value;
    }

    @Override
    public Optional<BaseOperation> getInsideOperation() {
        return previousOperation;
    }
}
