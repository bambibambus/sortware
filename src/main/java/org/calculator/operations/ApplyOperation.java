package org.calculator.operations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/*
* Apply operator class
* */
@NoArgsConstructor
@AllArgsConstructor
public class ApplyOperation implements BaseOperation {

    private Optional<BaseOperation> previousOperation;
    @Getter
    private Number value;

    public ApplyOperation(Optional previousOperation) {
        this.previousOperation = previousOperation;
    }

    @Override
    public Number invoke() {
        return (Float)this.value;
    }

    @Override
    public Optional<BaseOperation> getInsideOperation() {
        return previousOperation;
    }
}
