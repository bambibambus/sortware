package org.calculator.operations;

import java.io.Serializable;
import java.util.Optional;

/*
* Interface all operation class must implement
* */
public interface BaseOperation {

    Number invoke();

    Optional<BaseOperation> getInsideOperation();

}