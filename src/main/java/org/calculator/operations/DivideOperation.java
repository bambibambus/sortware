package org.calculator.operations;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.calculator.util.LambdaUtil;

import java.util.Optional;

import static org.calculator.util.LambdaUtil.*;

/*
* Divide operator class
* */
@AllArgsConstructor
@NoArgsConstructor
public class DivideOperation implements BaseOperation {

    private Optional<BaseOperation> previousOperation;

    @Getter
    private Number value;
    
    public DivideOperation(Optional<BaseOperation> previousOperation) {
        this.previousOperation = previousOperation;
    }


    @Override
    public Number invoke() {
        return tryNumericOrThrowRuntime(() ->
            this.previousOperation.isPresent() ? (Float)this.previousOperation.get().invoke() / (Float)this.value : (Float)this.value);
    }

    @Override
    public Optional<BaseOperation> getInsideOperation() {
        return previousOperation;
    }
}
