package org.calculator.operations;

import java.util.Arrays;
import java.util.List;

public enum Operator {
    ADD,
    SUBSTRACT,
    MULTIPLY,
    DIVIDE,
    APPLY,
    UNSUPPORTED;

    /*
    * Returns enum from string (by name)
    * @param String
    * @return Operator
    * */
    public static Operator getEnumByNameIgnoreCase(String name){
        List<Operator> operators = Arrays.asList(Operator.values());
        return operators
                .stream()
                .filter(operator -> operator.name().equalsIgnoreCase(name))
                .findAny()
                .orElse(Operator.UNSUPPORTED);
    }



}
