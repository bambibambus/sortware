package org.calculator.operations;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Optional;

/*
* Substract operator class
* */
@NoArgsConstructor
@AllArgsConstructor
public class SubstractOperation implements BaseOperation {

    private Optional<BaseOperation> previousOperation;
    @Getter
    private Number value;

    public SubstractOperation(Optional previousOperation){
        this.previousOperation = previousOperation;
    }

    @Override
    public Number invoke() {
        return this.previousOperation.isPresent() ? (Float)this.previousOperation.get().invoke() - (Float)this.value : -(Float)this.value;
    }

    @Override
    public Optional<BaseOperation> getInsideOperation() {
        return previousOperation;
    }
}
