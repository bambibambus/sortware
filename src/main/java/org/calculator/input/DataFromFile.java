package org.calculator.input;

import org.apache.commons.lang3.StringUtils;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.stream.Stream;

/*
*
* */
public class DataFromFile {

    private Path filePath;

    public DataFromFile(String fileName){
        if (StringUtils.isNotBlank(fileName)) {
            File file = new File(fileName);
            this.filePath = file.toPath();
        }else
            this.filePath = null;
    }

    public DataFromFile(Path filePath) {
        this.filePath = filePath;
    }

    /*
    * Returns stream of lines in file
    * @return Stream<String>
    * */
    public Stream<String> getAsStream(){
        return !Objects.isNull(filePath) ? lines(filePath) : Stream.empty();
    }

    public Stream<String> lines(Path path){
        try {
            return Files.lines(path);
        }catch (Exception e) {
            System.out.println("Error while processing file " + path.toString() + "\n file is invalid or does not exists");
            throw new RuntimeException(e);
        }
    }
}
