package org.calculator.input;

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.calculator.util.LambdaUtil.*;

public class ReadData {


    /*
    * Returns collection of operations from DataFromFile object
    * @param DataFromFile
    * @returns Collection<Entry>
    * */
    public static Collection<Entry> getAsOperatorCollection(DataFromFile dataFromFile) {
        return Objects.nonNull(dataFromFile) ?
                returnList(dataFromFile.getAsStream().collect(Collectors.toList())) :
                Lists.newLinkedList();
    }

    public static Collection<Entry> returnList(Collection<String> list) {
        return isValid(list.stream()) ? toList(list.stream()): Lists.newLinkedList();
    }

    /*
    * Returns collection of entries from stream
    * @param Stream<String>
    * @return Collection
    * */
    public static Collection toList(Stream<String> asStream) {
        return toStreamOfList(asStream)
                .map(ReadData::toEntry)
                .collect(Collectors.toList());
    }

    /*
    * Checks if lines in file are two strings
    * @param Stream<String>
    * @return boolean
    * */
    private static boolean isValid(Stream<String> stream){
        return  !toStreamOfList(stream)
                .anyMatch(list -> list.size() != 2);
    }

    /*
    * Stream of two elements list {operator, argument}
    * @param Stream<String>
    * @return Stream<List<String>>
    * */
    private static Stream<List<String>> toStreamOfList(Stream<String> stream) {
        return stream
                .map(line -> Splitter
                        .on(CharMatcher.BREAKING_WHITESPACE)
                        .trimResults()
                        .omitEmptyStrings()
                        .splitToList(line));
    }

    /*
    * Converts list {operatrion, argument} to map entry
    * @param List<String>
    * @return Map.Entry<Operator,Number>
    * */
    private static Map.Entry<Operator,Number> toEntry(List<String> twoElementsList){
        Operator operator = Operator.getEnumByNameIgnoreCase(twoElementsList.get(0));
        Number number = tryOrThrowRuntime(() -> new Float(twoElementsList.get(1)));
        return new Entry<>(operator, number);
    }

    /*
    * Checks if APPLY operation is last operation in file
    * @param Collection<Entry>
    * @retrun boolean
    * */
    public static boolean isApplyLastOperation(Collection<Entry> listOfEntries) {
        return  getLastEntryOptional(listOfEntries)
                .filter(entry -> entry.getKey().equals(Operator.APPLY))
                .isPresent();
    }

    /*
    * Returns Optional of last entry in collection
    * @param Collection<Entry>
    * @return Optional<Entry>
    * */
    public static Optional<Entry> getLastEntryOptional(Collection<Entry> listOfEntries){
        return Optional.ofNullable(listOfEntries)
                .filter(list -> list.size() > 0)
                .map(Iterables::getLast);
    }
}
