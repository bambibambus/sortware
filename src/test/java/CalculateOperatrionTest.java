import org.assertj.core.util.Lists;
import org.calculator.logic.CalculateOperation;
import org.calculator.operations.AddOperation;
import org.calculator.operations.ApplyOperation;
import org.calculator.operations.BaseOperation;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class CalculateOperatrionTest {


    private static CalculateOperation calculateOperation;

    @Before
    public void initalize(){
        if(calculateOperation == null)
            calculateOperation = new CalculateOperation(Lists.emptyList());
    }

    @Test
    public void shouldReturnCorrectStructure(){
        //given
        Collection listOFEntries = Arrays.asList(
                new Entry<>(Operator.ADD, 2F),
                new Entry<>(Operator.APPLY, 10F)
        );
        //when
        BaseOperation resultStructure = calculateOperation.createLinkedOperations(listOFEntries);

        //then
        assertThat(resultStructure).isNotNull().isInstanceOf(AddOperation.class);
        assertThat(resultStructure.getInsideOperation().get()).isNotNull().isInstanceOf(ApplyOperation.class);
    }

    @Test
    public void shouldCalculateApply(){
        //given
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                new Entry(Operator.APPLY, 2.456F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        assertThat(result).isEqualTo(2.456F);
    }

    @Test
    public void shouldCalculateAdd(){
        //given
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.APPLY, 10F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        assertThat(result).isEqualTo(12F);
    }

    @Test
    public void shouldCalculateSubstract(){
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.SUBSTRACT, 2F),
                        new Entry<>(Operator.APPLY, 10F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        assertThat(result).isEqualTo(8F);
    }
    @Test
    public void shouldCalculateMultiple(){
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.MULTIPLY, 2.5F),
                        new Entry<>(Operator.APPLY, 10F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        assertThat(result).isEqualTo(25F);
    }
    @Test
    public void shouldCalculatedDivide(){
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.DIVIDE, 2.5F),
                        new Entry<>(Operator.APPLY, 10F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        assertThat(result).isEqualTo(4F);
    }


}
