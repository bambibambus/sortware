import com.google.common.collect.Lists;
import org.calculator.logic.CalculateOperation;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.List;


import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class SplitOperationsTest {


    @Test
    public void shouldSplitListOfOperations(){
        //given
        List expectedResult = Arrays.asList(
                Arrays.asList(
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.MULTIPLY, 10F),
                        new Entry<>(Operator.APPLY, 30F)),
                Arrays.asList(
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.MULTIPLY, 3.5F),
                        new Entry<>(Operator.SUBSTRACT, 16F),
                        new Entry<>(Operator.APPLY, 5F)),
                Arrays.asList(
                        new Entry<>(Operator.MULTIPLY, 3.5F),
                        new Entry<>(Operator.SUBSTRACT, 16F),
                        new Entry<>(Operator.APPLY, 0F))
        );
        List input = Arrays.asList(
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.MULTIPLY, 10F),
                        new Entry<>(Operator.APPLY, 30F),
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.MULTIPLY, 3.5F),
                        new Entry<>(Operator.SUBSTRACT, 16F),
                        new Entry<>(Operator.APPLY, 5F),
                        new Entry<>(Operator.MULTIPLY, 3.5F),
                        new Entry<>(Operator.SUBSTRACT, 16F),
                        new Entry<>(Operator.APPLY, 0F)
        );
        CalculateOperation calculateOperation = new CalculateOperation(Lists.newArrayList());
        //when
        List result = (List) calculateOperation.splitByApply(input);
        //then
        assertThat(result).isNotEmpty().hasSize(3).containsAll(expectedResult);

    }
}
