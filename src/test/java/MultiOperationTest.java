import org.assertj.core.data.Offset;
import org.assertj.core.util.Lists;
import org.calculator.logic.CalculateOperation;
import org.calculator.operations.BaseOperation;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class MultiOperationTest {

    private static CalculateOperation calculateOperation;

    @Before
    public void initalize(){
        if(calculateOperation == null)
            calculateOperation = new CalculateOperation(Lists.emptyList());
    }

    @Test
    public void shouldPassMultiOperatrion(){
        //given
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.ADD, 2F),
                        new Entry<>(Operator.MULTIPLY, 3.5F),
                        new Entry<>(Operator.SUBSTRACT, 16F),
                        new Entry<>(Operator.APPLY, 10F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        //SHOULD RETURN mathematical precedence 10 + 2 * 3.5 - 16 = 26
        assertThat(result).isEqualTo(26F);
    }

    @Test
    public void shouldPassMultiOperatrion1(){
        //given
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.ADD, 4.5F),
                        new Entry<>(Operator.MULTIPLY, 1.5F),
                        new Entry<>(Operator.SUBSTRACT, 6F),
                        new Entry<>(Operator.DIVIDE, 32F),
                        new Entry<>(Operator.ADD, 0.95F),
                        new Entry<>(Operator.APPLY, 8F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        //SHOULD RETURN mathematical precedence 8 + 4.5 * 1.5 - 6 / 32.11 + 0.95 = 1.3484375
        assertThat(result).isEqualTo(1.3484375F, Offset.offset(0.001F));
    }

    @Test
    public void shouldPassMultiOperatrion2(){
        //given
        BaseOperation operation = calculateOperation.createLinkedOperations(Arrays.asList(
                        new Entry<>(Operator.ADD, 4.5F),
                        new Entry<>(Operator.MULTIPLY, 8F),
                        new Entry<>(Operator.ADD, 6.4F),
                        new Entry<>(Operator.DIVIDE, 2F),
                        new Entry<>(Operator.SUBSTRACT, 30F),
                        new Entry<>(Operator.MULTIPLY, 4.5F),
                        new Entry<>(Operator.MULTIPLY, 1.5F),
                        new Entry<>(Operator.SUBSTRACT, 5F),
                        new Entry<>(Operator.DIVIDE, 5F),
                        new Entry<>(Operator.ADD, 1F),
                        new Entry<>(Operator.APPLY, 18F))
        );
        //when
        Float result = (Float) operation.invoke();
        //then
        //SHOULD RETURN mathematical precedence 18 + 4.5 * 8 + 6.4 / 2 - 30 *4.5 * 1.5 - 5 / 5 + 1 = 85.32
        //using offset due to numerical errors
        assertThat(result).isEqualTo(85.32F, Offset.offset(0.001F));
    }
}
