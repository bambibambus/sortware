import org.assertj.core.util.Lists;
import org.calculator.input.DataFromFile;
import org.calculator.logic.CalculateOperation;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Collection;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class IntegrationTest {

    private static CalculateOperation calculateOperation;
    @Mock
    DataFromFile dataFromFile;

    @Before
    public void initalize(){
        if(calculateOperation == null)
            calculateOperation = new CalculateOperation(Lists.emptyList());
    }

    @Test
    public void shouldReturnResultFronInput(){
        //given
        given(dataFromFile.getAsStream()).willReturn(
                Stream.<String>builder()
                        .add("add 2")
                        .add("multiply 10")
                        .add("apply 30")
                        .add("multiply 3.5")
                        .add("substract 16")
                        .add("apply 5")
                        .add("multiply 3.5")
                        .add("substract 30")
                        .add("divide 30")
                        .add("apply 0")
                        .build());
        //when
        Collection result = calculateOperation.calculateFromInput(dataFromFile);
        //then
        //should return list of three values: 320;1.5;-1
        assertThat(result).isNotEmpty().hasSize(3).containsAll(Lists.newArrayList(320F,1.5F,-1F));
    }

}
