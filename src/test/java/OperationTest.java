
import org.calculator.logic.OperationFactory;
import org.calculator.operations.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Optional;

import static org.assertj.core.api.Assertions.*;

@RunWith(MockitoJUnitRunner.class)
public class OperationTest {

    private static boolean wasSetup = false;

    private static OperationFactory operationFactory;

    @Before
    public void initalizeVariables(){
        if(!wasSetup) {
            this.operationFactory = new OperationFactory();
            wasSetup = true;
        }
    }

    @Test
    public void shouldReturnCorrectInstances(){
        //given
        BaseOperation addOperation = operationFactory.get(Optional.empty(),Operator.ADD, 10F).get();
        BaseOperation applyOperation = operationFactory.get(Optional.empty(),Operator.APPLY, 10F).get();
        BaseOperation substractOperation = operationFactory.get(Optional.empty(),Operator.SUBSTRACT, 10F).get();
        BaseOperation multipleOperation = operationFactory.get(Optional.empty(),Operator.MULTIPLY, 10F).get();
        BaseOperation divideOperation = operationFactory.get(Optional.empty(),Operator.DIVIDE, 10F).get();
        //when
        //then
        assertThat(addOperation).isInstanceOf(AddOperation.class);
        assertThat(applyOperation).isInstanceOf(ApplyOperation.class);
        assertThat(substractOperation).isInstanceOf(SubstractOperation.class);
        assertThat(multipleOperation).isInstanceOf(MultiplyOperation.class);
        assertThat(divideOperation).isInstanceOf(DivideOperation.class);
    }

    @Test
    public void shouldReturnIdentity(){
        //given
        BaseOperation operation = operationFactory.get(Optional.empty(),Operator.APPLY, 10F).get();
        //when
        Number result = operation.invoke();
        //then
        assertThat(result).isEqualTo(10F);
    }

}
