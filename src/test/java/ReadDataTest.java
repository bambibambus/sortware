import com.google.common.collect.Lists;
import org.calculator.input.DataFromFile;
import org.calculator.input.ReadData;
import org.calculator.operations.Operator;
import org.calculator.util.Entry;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.given;

@RunWith(MockitoJUnitRunner.class)
public class ReadDataTest {

    @Mock
    DataFromFile dataFromFile;

    @Test
    public void shouldReadStreamToList(){
        //given
        given(dataFromFile.getAsStream()).willReturn(
                Stream.<String>builder()
                        .add("add 2")
                        .add("multiply 10")
                        .build());
        //when
        Collection<Entry> result = ReadData.getAsOperatorCollection(dataFromFile);
        //then
        assertThat(result)
                .containsAll(Arrays.asList(new Entry<>(Operator.ADD, 2F), new Entry<>(Operator.MULTIPLY, 10F)));

    }

    @Test
    public void shouldReturnTrueApplyLastOperation(){
        //given
        Collection listOFEntries = Arrays.asList(
                new Entry<>(Operator.ADD, 2F),
                new Entry<>(Operator.MULTIPLY, 10F),
                new Entry<>(Operator.APPLY, 30F)
        );
        //when
        boolean result = ReadData.isApplyLastOperation(listOFEntries);
        //then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseApplyNotLastOperation(){
        //given
        Collection listOFEntries = Arrays.asList(
                new Entry<>(Operator.ADD, 2F)
        );
        //when
        boolean result = ReadData.isApplyLastOperation(listOFEntries);
        boolean resultEmptyList = ReadData.isApplyLastOperation(Lists.newArrayList());
        //then
        assertThat(result).isFalse();
        assertThat(resultEmptyList).isFalse();
    }
}
